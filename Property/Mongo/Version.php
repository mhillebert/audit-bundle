<?php
namespace MHillebert\AuditBundle\Property\Mongo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Version
 * Coupled with using audit, this provides a version number to the properties that use this trait
 * eg. use \MHillebert\AuditBundle\Property\Version;
 *
 * This will add a version column to your entity's table
 *
 *
 * @package MHillebert\AuditBundle\Property\Version
 */
trait Version
{
    /**
     * @var int
     * @MongoDB\Field(type="int", nullable=false, options={"default": 0})
     * @MongoDB\Version
     * @MHillebert\Audit
     */
    protected $version = 0;

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return $this
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
        return $this;
    }
}