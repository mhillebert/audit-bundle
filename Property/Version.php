<?php
namespace MHillebert\AuditBundle\Property;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Version
 * Coupled with using audit, this provides a version number to the properties that use this trait
 * eg. use \MHillebert\AuditBundle\Property\Version;
 *
 * This will add a version column to your entity's table
 *
 *
 * @package MHillebert\AuditBundle\Property\Version
 */
trait Version
{
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     * @ORM\Version
     * @MHillebert\Audit
     */
    protected $version;

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return $this
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
        return $this;
    }
}