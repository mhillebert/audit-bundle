<?php
namespace MHillebert\AuditBundle\Exception;

/**
 * Class Audit
 * @author Mark Hillebert
 * @package MHillebert\Exception
 */
class Audit extends \Exception
{
    /**
     * @var string Default Error Message
     */
    private static $_message = "\\MHillebert\\Exception\\Audit has been thrown";

    /**
     * Constructor
     *
     * @param null       $message
     * @param int        $code
     * @param \Exception $previous
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        if (!$message) {
            $message = self::$_message;
        }
        parent::__construct($message, $code = 0, $previous = null);
    }
}