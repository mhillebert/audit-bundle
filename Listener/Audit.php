<?php
namespace MHillebert\AuditBundle\Listener;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Entity;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Doctrine\Common\Util\ClassUtils;
use \Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
/**
 * Class Audit
 * @author Mark Hillebert
 * @package MHillebert\AuditBundle\Listener
 */
class Audit
{
    /**
     * Allowed Values
     */
    const VALUE_CREATE = 'create';
    const VALUE_UPDATE = 'update';
    const VALUE_DELETE = 'delete';
    const VALUE_READ = 'read';
    const VALUE_LOGIN = 'login'; // Not sure if this should be here. Seems useful, but probably a better way.


    const UNKNOWN_ID = "<unknown>";

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var \ReflectionClass
     */
    private $reflectionClass;

    /**
     * @var \Doctrine\Common\Annotations\AnnotationReader
     */
    protected $annotationReader;

    /**
     * Audit constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage, RequestStack $requestStack)
    {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $this->updateVersionIfApplicable($event->getObjectManager(), $event->getObject());
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(\Doctrine\Common\Persistence\Event\LifecycleEventArgs $event)
    {
        $this->updateVersionIfApplicable($event->getObjectManager(), $event->getObject());
    }

    /**
     * @param object $entity
     */
    protected function updateVersionIfApplicable(ObjectManager $em, $entity)
    {
        if (!$this->isEntity($em, $entity)) {
            throw new \MHillebert\AuditBundle\Exception\Audit("Must be an Entity." );
        }
        if ($this->isAuditClass($entity) && property_exists($entity, 'version')) {
            // need to increment the version
            $entity->setVersion($entity->getVersion() + 1);
        }
    }

    /**
     * Track updates to an existing entity
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($this->isAuditClass($entity)) {
            $audit = $this->createAudit($event, self::VALUE_UPDATE);
            $changes = $event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
            if ($changes) {
                $auditedChanges = array();

                foreach ($changes as $property => $change) {
                    if ($this->isAuditProperty($entity, $property)) {
                        $auditedChanges[$property] = $this->getValue($changes[$property][0], $changes[$property][1], $entity, $property, $event->getEntityManager());
                    }
                    if ($property === 'version') {
                        $audit->setVersion($auditedChanges[$property]);
                        unset($auditedChanges[$property]);
                    }
                }

                if ($audit && $auditedChanges) {
                    $audit->setChanges($auditedChanges);
                }
                if ($audit) {
                    $event->getEntityManager()->persist($audit);
                    $event->getEntityManager()->flush($audit);
                }
            }else {
                if ($entity instanceof \App\Document\Week) { ddd('no changes');}

            }

        }
    }

    /**
     * Track creation of new entity
     * @param LifecycleEventArgs $event
     */
    public function postPersist(\Doctrine\Common\Persistence\Event\LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if ($this->isAuditClass($entity)) {
            $audit = $this->createAudit($event, self::VALUE_CREATE);
            if (property_exists($entity, 'version')) {
                $audit->setVersion(0); // always started at version 0 on creation. // the entity is now at version 1.
            }
            if ($audit) {
                $event->getObjectManager()->persist($audit);
                $event->getObjectManager()->flush($audit);
            }
        }
    }

    /**
     * Track deletions of existing entity
     * @param LifecycleEventArgs $event
     * @throws \Exception
     */
    public function preRemove(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($this->isAuditClass($entity)) {
            $ref = $this->getReflectionClass($entity);
            $props = $ref->getProperties();
            foreach ($props as $prop) {
                if ($this->isAuditProperty($entity, $prop->name)) {
                    $auditedChanges[$prop->name] = $this->getValue($entity->{'get' . ucfirst($prop->name)}(), null, $entity, $prop->name, $event->getEntityManager());
                }
            }

            $audit = $this->createAudit($event, self::VALUE_DELETE);
            if ($audit) {
                if (isset($auditedChanges)) {
                    $audit->setChanges($auditedChanges);
                }
                if (property_exists($entity, 'version')) {
                    $audit->setVersion($entity->getVersion()); // always started at version 0 on creation. // the entity is now at version 1.
                }
                $event->getEntityManager()->persist($audit);
                $event->getEntityManager()->flush($audit);
            }
        }
    }

    /**
     * Track reads (loading) of entity
     * @param LifecycleEventArgs $event
     * @throws \MHillebert\AuditBundle\Exception\Audit
     */
    public function postLoad(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if ($this->isAuditClass($entity)) {
            $audit = $this->createAudit($event, self::VALUE_READ);
            if ($audit) {
                $event->getEntityManager()->persist($audit);
                $event->getEntityManager()->flush($audit);
            }
        }
    }

    /**
     * @param \Doctrine\Common\Persistence\Event\LifecycleEventArgs $event
     * @param $type
     * @return \MHillebert\AuditBundle\Entity\Audit|null
     * @throws \MHillebert\AuditBundle\Exception\Audit
     */
    protected function createAudit(\Doctrine\Common\Persistence\Event\LifecycleEventArgs $event, $type)
    {
        $entity = $event->getEntity();
        if (!$this->isEntity($event->getObjectManager(), $entity)) {
            throw new \MHillebert\AuditBundle\Exception\Audit("model must be a doctrine entity.");
        }

        $ar = $this->getAnnotationReader();
        $ref = new \ReflectionClass($entity);
        $class = $ar->getClassAnnotation($ref, 'MHillebert\AuditBundle\Annotation\EntityAudit');

        // If this event type is not an event to audit, then just return
        if ($class && !in_array($type, $class->events)) {
            return null;
        }


        $audit = new \MHillebert\AuditBundle\Entity\Audit();
        if ($event instanceof \Doctrine\ODM\MongoDB\Event\LifecycleEventArgs) {
            $audit = new \MHillebert\AuditBundle\Document\Audit();
        }
        $audit->setAction($type);
        $audit->setDateChanged(new \DateTime()); // This should change to make sure this is a standard time (not user timezone)?
        $audit->setModelClass(get_class($entity));

        if ($this->tokenStorage->getToken() && null !== $this->tokenStorage->getToken()->getUsername()) {
            $audit->setUserId($this->tokenStorage->getToken()->getUsername());
        } else {
            $audit->setUserId(self::UNKNOWN_ID);
        }
        if ($this->requestStack->getCurrentRequest() && $this->requestStack->getCurrentRequest()->getClientIp()) {
            $audit->setUserIp($this->requestStack->getCurrentRequest()->getClientIp());
        } else {
            $audit->setUserIp(self::UNKNOWN_ID);
        }

        $idProperty = $this->findIdProperty($entity);
        if ($idProperty) {
            $audit->setModelId($entity->{'get' . ucfirst($idProperty)}());
        } else {
            $audit->setModelId("<unknown>");
        }

        return $audit;
    }

    /**
     * A helper function that attempts to find a property that looks like it is being used as identification.
     * First it looks for the AuditId annotation, if not found then looks for doctrine's Id Annotation.
     *
     * @param $entity
     * @return null|string
     */
    protected function findIdProperty($entity)
    {
        $ar = $this->getAnnotationReader();
        $ref = new \ReflectionClass($entity);
        $props = $ref->getProperties();
        $idProperty = $auditIdProperty = $fallbackIdProperty = null;
        foreach ($props as $prop) {
            // Try to find the id property, which is the one marked as @ORM\Id
            if ($ar->getPropertyAnnotation($prop, 'Doctrine\ORM\Mapping\Id')) {
                $fallbackIdProperty = $prop->name;
            }

            // Try to find the id property, which is the one marked as @MongoDB\Id
            if ($ar->getPropertyAnnotation($prop, 'Doctrine\ODM\MongoDB\Mapping\Annotations\Id')) {
                $fallbackIdProperty = $prop->name;
            }

            if ($ar->getPropertyAnnotation($prop, 'MHillebert\AuditBundle\Annotation\AuditId')) {
                $auditIdProperty = $prop->name;
                break; // We take the first encounter of this annotation as the intended Id for the entity.
            }
        }

        // Use the correct Id property. AuditId annotation trumps Doctrine's @Id
        if ($auditIdProperty) {
            $idProperty = $auditIdProperty;
        } elseif ($fallbackIdProperty) {
            $idProperty = $fallbackIdProperty;
        }

        return $idProperty;
    }

    /**
     * A helper function that attempts to find the old value. If it's an collection then it tries to see what was
     * added/removed by the entity's id property and class. This is meant to provide a concise change without storing
     * the whole entity within the audit changes property.
     * @param $oldValue
     * @param $newValue
     * @param $entity
     * @param $property
     * @param $manager
     * @return array|string
     */
    protected function getValue($oldValue, $newValue, $entity, $property, $manager)
    {
        switch (gettype($oldValue)) {
            case 'object':
                switch (get_class($oldValue)) {
                    case 'DateTime':
                        $audit = json_encode($oldValue);
                        break;
                    default:
                        if (is_object($oldValue) && $oldValue instanceof \Doctrine\Common\Collections\ArrayCollection) {
                            $multi = [];
                            if (is_object($newValue) && $newValue instanceof \Doctrine\Common\Collections\ArrayCollection) {
                                $removed = array_diff($oldValue->toArray(), $newValue->toArray());
                                foreach ($removed as $subValue) {
                                    $idProperty = $this->findIdProperty($subValue);
                                    $class = get_class($subValue);
                                    $multi['removed'] = [
                                        'id' => $idProperty ? $subValue->{'get' . $idProperty}() : self::UNKNOWN_ID,
                                        'class' => $class ?: self::UNKNOWN_ID
                                    ];
                                }
                                $added = array_diff($newValue->toArray(), $oldValue->toArray());
                                foreach ($added as $subValue) {
                                    $idProperty = $this->findIdProperty($subValue);
                                    $class = get_class($subValue);
                                    $multi['added'] = [
                                        'id' => $idProperty ? $subValue->{'get' . $idProperty}() : self::UNKNOWN_ID,
                                        'class' => $class ?: self::UNKNOWN_ID
                                    ];
                                }
                            } else {
                                // the new value is something other than an array collection
                                foreach ($oldValue as $subValue) {
                                    $idProperty = $this->findIdProperty($subValue);
                                    $class = get_class($subValue);
                                    $multi[] = [
                                        'id' => $idProperty ? $subValue->{'get' . $idProperty}() : self::UNKNOWN_ID,
                                        'class' => $class ?: self::UNKNOWN_ID
                                    ];
                                }
                            }

                            $sValue = $multi;
                        } elseif ($this->isEntity($manager, $oldValue)) {
                            $idProperty = $this->findIdProperty($oldValue);
                            $class = get_class($oldValue);
                            $sValue = [
                                'id' => $idProperty ? $oldValue->{'get' . $idProperty}() : self::UNKNOWN_ID,
                                'class' => $class ?: self::UNKNOWN_ID
                            ];
                        } else {
                            $sValue = $entity->{'get' . ucfirst($property)}();
                        }
                        $audit = $sValue;
                }
                break;
            case 'array':
            default:
                $audit = $oldValue;
        }

        return $audit;
    }

    /**
     * A helper function to determine if the Entity is a doctrine entity. This audit package only works with doctrine.
     * @param \Doctrine\ORM\EntityManager $em
     * @param string|object $class
     *
     * @return boolean
     */
    protected function isEntity(ObjectManager $em, $class)
    {
        if (is_object($class)) {
            $class = ClassUtils::getClass($class);
        }
        return !$em->getMetadataFactory()->isTransient($class);
    }

    /**
     * A helper function to determine if the property contains the Audit annotation
     * @param object $entity
     * @param string $property
     * @return null|object
     */
    protected function isAuditProperty($entity, $property)
    {
        $ar = $this->getAnnotationReader();
        $ref = $this->getReflectionClass($entity);
        $refProp = $ref->getProperty($property);
        return $ar->getPropertyAnnotation($refProp, 'MHillebert\AuditBundle\Annotation\Audit');
    }

    /**
     * A helper function to determine if the Entity is to be audited
     * @param $entity
     * @return null|object
     */
    protected function isAuditClass($entity)
    {
        $ar = $this->getAnnotationReader();
        $ref = $this->getReflectionClass($entity);
        $value = $ar->getClassAnnotation($ref, 'MHillebert\AuditBundle\Annotation\EntityAudit');
        return $value;
    }

    /**
     * A helper function to ensure a singleton annotation reader
     * @return AnnotationReader
     */
    protected function getAnnotationReader()
    {
        if (!$this->annotationReader) {
            $this->annotationReader = new AnnotationReader();
        }

        return $this->annotationReader;
    }

    /**
     * A helper function to ensure a singleton reflection class
     * @param $entity
     * @return \ReflectionClass
     */
    protected function getReflectionClass($entity)
    {
        $this->reflectionClass = new \ReflectionClass($entity);

        return $this->reflectionClass;
    }

}