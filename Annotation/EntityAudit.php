<?php
namespace MHillebert\AuditBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Attribute;
use Doctrine\Common\Annotations\Annotation\Attributes;
use Doctrine\Common\Annotations\AnnotationException;

/**
 * @class Audit
 * @author Mark Hillebert
 * @Annotation
 * @Target("CLASS")
 * @Attributes({
 *   @Attribute("events", type = "array")
 * })
 */
class EntityAudit
{
    /**
     * @var array
     */
    public $events;

    /**
     * EntityAudit constructor.
     * @param array|null $values
     * @throws AnnotationException
     */
    public function __construct(array $values = null)
    {
        $knownEvents = ['create', 'read', 'update', 'delete'];
        $defaultEvents = ['create', 'update', 'delete'];
        if ($values && isset($values['events'])) {
            if (count(array_diff($values['events'], $knownEvents)) > 0) {
                throw new AnnotationException("Unknown event in entity audit annotation. Please use only create, read, update, and/or delete.");
            }
            $this->events = $values['events'];
        }else{
            $this->events = $defaultEvents;
        }

    }
}