<?php
namespace MHillebert\AuditBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @class Audit
 * @author Mark Hillebert
 * @Annotation
 * @Target("PROPERTY")
 */
class Audit
{
}