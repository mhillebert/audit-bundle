AuditBundle
=============

The `AuditBundle` provides integration of an entity audit trail library into the Symfony2 framework.

An audit trail track the changes to an entity's properties using the `@MHillebert/Audit` annotation.
When a property has this annotation, the changes can be tracked on postPersist, postUpdate, and preRemove, and postLoad.
The property changes are tracked as follows:
- basic types: whichever type (int, string, bool, etc)
- Entity: the id of the entity and the class in a json encoded array

coming soon --- versioning. yes, versioning.

Installation
============
Add this to the require section of your project's composer.json file:
```
"require": {
    "mhillebert/audit-bundle": "dev-master"
}
```

Also, until this is uploaded to packagist, you will need to tell composer where to find this repository like this:
```
"repositories": [
     {
         "type": "vcs",
         "url": "https://bitbucket.org/mhillebert/audit-bundle.git"
     }],
```
Next, run `composer install` to download.

Finally, you will need to run doctrine's schema update tool to create the audit table within your database. If you're using symfony, the command is `php bin/console doctrine:schema:update --force`

Usage
=====
Within any or all of your entities add the following use statement:
```
use MHillebert\AuditBundle\Annotation as MHillebert;
```

Then within your class doc-block add the following:
```
@MHillebert\EntityAudit()
```
Finally, add this to any property you wish to be evaluated for inclusion in the audit change:
```
 @MHillebert\Audit
```
Documentation
============
###Versioning
If you wish to track your changes with versioning, then simply add the trait to your class along with the annotations below.
`use \MHillebert\AuditBundle\Property\Version;`

If you are creating Entity Repositories for your entity then you will want to exend your class from
`MHillebert\AuditBundle\Repository\Versioned`.
This will give you access to additional revision methods.

###Annotations
####`@MHillebert\AuditEntity(array $events = ["create", "update", "delete"])` Annotation:

This annotation can take an optional array of events to listen for. These events are named in the CRUD naming convention: `"create"`, `"read"`, `"update"`, `"delete"`. 

If no array is given, it defaults to [`"create"`, `"update"`, `"delete"`].

####`@MHillebert\Audit`
This annotation informs the sudit listener which properties of the given Entity to include in the audit's changed property. The audit-bundle will attempt to intelligently capture only the necessary information when tracking changes:
+ Basic types (e.g. int, bool, string, etc), the entity of the old value will be captured.
+ Doctrine's ArrayCollections: the audit-bundle will attempt to track which are added and removed and only capture the entity identification and class.

####`@MHillebert\AuditId`
The audit-bundle will automatically look for your Entity's `@Id` annotation and use this when persisting an audit; however, occasionally you may need to specify which property other than the `@Id` property should be used and this is when the `@MHillebert\AuditId` annotation should be used.
 
 When the audit bundle is attempting to discover the Entity's id property, it will read the annotations of all properties and use the first `@MHillebert\AuditId` property, if no property contains this annotation, then the audit-bunsle will attempt to locate an id using Doctrines `@Id` annotation. **Your entity must have at least one property annotated as an Id.**


License
=======

This bundle is released under the [MIT license](LICENSE)