<?php
namespace MHillebert\AuditBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Audit
 * @package MHillebert\AuditBundle\Document
 * @author Mark Hillebert
 *
 * @MongoDB\Document(collection="_audits_", repositoryClass="MHillebert\AuditBundle\Repository\Mongo\Audit",
 *     indexes={
 *          @MongoDB\Index(keys={"version"="desc"}),
 *          @MongoDB\Index(keys={"modelClass"="desc"}),
 *          @MongoDB\Index(keys={"modelId"="desc"}),
 *   })
 */
class Audit
{
    /**
     * @var int
     *
     * @MongoDB\Id()
     */
    protected $id;

    /**
     * @var string The action taken on the entity
     * @MongoDB\Field(type="string")
     */
    protected $action;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    protected $userIp;

    /**
     * @var \DateTime The datetime of the audit
     *
     * @MongoDB\Field(type="date")
     */
    protected $dateChanged;

    /**
     * @var int
     * @MongoDB\Field(type="int", nullable=true)
     */
    protected $version;

    /**
     * @var string The class of the entity
     * @MongoDB\Field(type="string")
     */
    protected $modelClass;

    /**
     * @var string The Doctrine (@)Id property of the entity
     * @MongoDB\Field(type="string")
     */
    protected $modelId;

    /**
     * @var array An array of the old values of entity
     * @MongoDB\Field(type="collection", nullable=true)
     */
    protected $changes;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Audit
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Audit
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return Audit
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @param string $userIp
     * @return Audit
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateChanged()
    {
        return $this->dateChanged;
    }

    /**
     * @param \DateTime $dateChanged
     * @return Audit
     */
    public function setDateChanged($dateChanged)
    {
        $this->dateChanged = $dateChanged;
        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return $this
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @param string $modelClass
     * @return Audit
     */
    public function setModelClass($modelClass)
    {
        $this->modelClass = $modelClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getModelId()
    {
        return $this->modelId;
    }

    /**
     * @param string $modelId
     * @return Audit
     */
    public function setModelId($modelId)
    {
        $this->modelId = $modelId;
        return $this;
    }

    /**
     * @return array
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * @param array $changes
     * @return Audit
     */
    public function setChanges($changes)
    {
        $this->changes = $changes;
        return $this;
    }
}